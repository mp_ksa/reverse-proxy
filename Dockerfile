FROM node:12-alpine AS base
WORKDIR /usr/src/app

# ---- Dependencies ----
FROM base AS dependencies

COPY . .

# install node packages
RUN yarn install --production
RUN yarn build

# ---- Release ----
FROM alpine

WORKDIR /usr/src/app

COPY --from=dependencies /usr/src/app .

RUN chmod 0755 ./go-reverse-proxy
# RUN ls

# RUN cd go-reverse-proxy
# RUN ./go-reverse-proxy 

EXPOSE 8080

CMD ./go-reverse-proxy start