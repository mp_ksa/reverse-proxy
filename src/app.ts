import 'dotenv/config';
import { useHealthz } from './middleware/useHealtz';
import { Server } from 'http';
import { bootstrap, useAll } from './utils/bootstrap';
import { getPortFromConfig } from './utils/port';
import { Application } from 'express';
import { notFoundHandler, errorHandler } from './handlers/error';
import { proxyEngine } from './handlers/proxy';
import * as bodyParser from 'body-parser';
import { expressLogger } from './adapters/logger';

const port = getPortFromConfig();
const bootstrapHandler = (app: Application, server: Server) => {
  app.use(
    bodyParser.urlencoded({
      extended: true,
    }),
  );
  app.use(bodyParser.json());
  useHealthz(server);
  app.use(proxyEngine);
  app.use(notFoundHandler);
  app.use(errorHandler);
};

bootstrap(port, bootstrapHandler);
