import * as winston from 'winston';
import * as path from 'path';
import * as expressWinston from 'express-winston';

const transportFile = (filename: string, level: string) => {
  return new winston.transports.File({
    filename: path.join(process.cwd(), 'logs', `${filename}.log.json`),
    level,
  });
};

const transportConsoleSimple = new winston.transports.Console({
  format: winston.format.simple(),
});

export const loggerOptions = {
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.splat(),
    winston.format.json(),
  ),
  meta: true,
  transports: [
    transportConsoleSimple,
    transportFile('error', 'error'),
    transportFile('info', 'info'),
    transportFile('debug', 'debug'),
    transportFile('warning', 'warn'),
    transportFile('log', 'info')
  ],
  log: winston.log,
  debug: winston.debug,
  info: winston.info,
  warn: winston.warn,
  error: winston.error
};

export const logger = winston.createLogger(loggerOptions);
export const expressLogger = expressWinston.logger(loggerOptions);
