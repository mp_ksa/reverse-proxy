import { Request, Response, NextFunction } from 'express';
import * as util from 'util';
import { ErrorMessage } from '../messages/error';
import { logger } from '../adapters/logger';

export const notFoundHandler = (
  req: Request,
  res: Response,
  next: NextFunction,
) =>
  res
    .status(404)
    .send({ message: util.format(ErrorMessage.NotFound, req.method, req.url) });

export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  logger.error(err);
  res.send({ message: err.message });
};
