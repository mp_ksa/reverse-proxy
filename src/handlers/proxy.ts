import { Request, Response, Router } from 'express';
import { expressLogger, logger, loggerOptions } from '../adapters/logger';
import * as expressWinston from 'express-winston';
import {
    createProxyMiddleware,
    Filter,
    Options,
    RequestHandler,
  } from 'http-proxy-middleware';
const proxyData = require('../../data/proxies.json');

export const proxyJSON = (datas: any, proxyReq: any, req: Request, res: Response) => {
    if(datas.logs){
        console.info(req.method , req.path, res.statusCode, JSON.stringify(req.headers));
        // logger.info()
        expressWinston.logger(loggerOptions);
    }
}

const proxyRouter = Router();
proxyData.forEach((val) => {
    proxyRouter.get(
        `${val.url}`,
        createProxyMiddleware({
            target: `${val.target}`,
            changeOrigin: val.changeOrigin,
            pathRewrite: val.rewrite,
            onProxyReq: (proxyReq, req, res) => {
                proxyJSON(val, proxyReq, req, res)
            },
            logLevel: val.logLevel,
            logProvider: (provider)=> {
                const logging = new (require('winston').createLogger)(loggerOptions);
                return logging;
            },
            onError: (err, req, res) => {
                // console.log(err);
            }
          })
      ); 
  });

export const proxyEngine = proxyRouter;