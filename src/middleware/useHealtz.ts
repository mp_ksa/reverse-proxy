import { createTerminus } from '@godaddy/terminus';
import { healthzHandler } from '../handlers/healtz';

const onSignal = async () => {};

export const useHealthz = (server: any) => {
  createTerminus(server, {
    signal: 'SIGINT',
    healthChecks: { '/healthz': healthzHandler },
    onSignal,
  });
};
