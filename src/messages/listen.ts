export enum ListenMessage {
  OnServerListening = 'Reverse Proxy listening on port %s',
}
