export enum ErrorMessage {
  NotFound = 'Resource %s %s not found',
}
