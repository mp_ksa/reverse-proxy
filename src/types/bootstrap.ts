import { Server } from 'http';
import { Application } from 'express';

export type BootstrapHandler = (app: Application, server: Server) => void;
