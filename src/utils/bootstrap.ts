import * as http from 'http';
import * as express from 'express';
import { BootstrapHandler } from '../types/bootstrap';
import { ListenMessage } from '../messages/listen';
import { logger } from '../adapters/logger';

const app = express();
const server = http.createServer(app);
export const bootstrap = (port: number, handler: BootstrapHandler) => {
  handler(app, server);
  server
    .listen(port)
    .on('listening', () => logger.info(ListenMessage.OnServerListening, port));
};

export const useAll = (app: express.Application) => {};
