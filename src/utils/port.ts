import * as config from 'config';

export const getPortFromConfig = (defaultPort: number = 9090) =>
  parseInt(config.get('port'), 10) || defaultPort;
